#include <iostream>

using namespace std;

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		cout << "X:" << x << endl;
		cout << "Y:" << y << endl;
		cout << "Z:" << z << endl;
		cout << "Vector length: " << sqrt(x*x + y*y + z*z) << endl;
	}

private:
	double x;
	double y;
	double z;
};

int main()
{
	Vector vector(24, 255, 72);
	vector.Show();
}